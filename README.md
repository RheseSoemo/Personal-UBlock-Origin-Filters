My personal U-Block Origin settings. Some of them get rid of advertisements and annoying popups that slip past default U-Block Origin settings. Others get rid of distractions online so I can be a more productive and happy person.

I have this repo set to public so I can easily access it on any of my devices device, even if I don't have access to my personal GitLab account. 

If you want to use it to, feel free to. Simply Download the "Ublock Filters.txt" and import them into your instance of [U-Block Origin](https://ublockorigin.com/).  

- Repository's icon was generated with [Craiyon](https://www.craiyon.com/)