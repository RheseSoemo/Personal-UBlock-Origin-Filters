! ==================================== !
! =========== FILE DOMAINS =========== !
! ==================================== !
! How moronic can you get Google?
*.zip
*.mov




! ======================================= !
! =========== GEEKS FOR GEEKS =========== !
! ======================================= !
! If you're going to bug me about sign ups and ad blockers than this is what you get

! ===== All Pop Ups ===== !
www.geeksforgeeks.org/wp-content/themes/iconic-one/js/gfg.min.js 
  ! Sourced From: https://www.reddit.com/r/programming/comments/q0oqai/what_is_wrong_with_geeksforgeeks_why_forcing_to/




! ================================ !
! =========== SVG REPO =========== !
! ================================ !
! I'll give to SVG Repo, they know their audience is technical enough to have ad-blockers, but can they get around this?

! ===== Banner Ads ===== !
www.svgrepo.com##.style_nativeInline__5nS3E
www.svgrepo.com##.style_nativeContentImage__35rBv
www.svgrepo.com##.style_nativeText__p0KQv
www.svgrepo.com##.style_flexbarClose__CrOf8

! ===== Footer Sponsorships ===== !
www.svgrepo.com##.style_sponsor__cAp_y




! =============================== !
! =========== YOUTUBE =========== !
! =============================== !
! Outsource my self-control so I can be lazier, also cause everything is so cluttered and annoying to use

! ===== Full Site Lockdown ===== !
!www.youtube.com

! ===== Shorts ===== !
www.youtube.com##ytd-guide-renderer a.yt-simple-endpoint path[d^="M10 14.65v-5.3L15 12l-5 2.65zm7.77-4.33"]:upward(ytd-guide-entry-renderer)
www.youtube.com##ytd-mini-guide-renderer a.yt-simple-endpoint path[d^="M10 14.65v-5.3L15 12l-5 2.65zm7.77-4.33"]:upward(ytd-mini-guide-entry-renderer)
www.youtube.com##ytd-browse #dismissible ytd-rich-grid-slim-media[is-short]:upward(ytd-rich-section-renderer)
www.youtube.com##ytd-browse[page-subtype="home"] .ytd-thumbnail[href^="/shorts/"]:upward(ytd-rich-item-renderer)
www.youtube.com##ytd-browse[page-subtype="subscriptions"] .ytd-thumbnail[href^="/shorts/"]:upward(ytd-grid-video-renderer,ytd-rich-item-renderer)
www.youtube.com##ytd-search .ytd-thumbnail[href^="/shorts/"]:upward(ytd-video-renderer)
www.youtube.com##ytd-watch-next-secondary-results-renderer .ytd-thumbnail[href^="/shorts/"]:upward(ytd-compact-video-renderer,ytd-shelf-renderer)
www.youtube.com##ytd-watch-next-secondary-results-renderer ytd-reel-shelf-renderer
www.youtube.com##ytd-browse[page-subtype="subscriptions"] ytd-video-renderer .ytd-thumbnail[href^="/shorts/"]:upward(ytd-item-section-renderer)
www.youtube.com##ytd-browse[page-subtype="channels"] #contents.ytd-reel-shelf-renderer:upward(ytd-item-section-renderer)
www.youtube.com##ytd-browse[page-subtype="trending"] .ytd-thumbnail[href^="/shorts/"]:upward(ytd-video-renderer)
www.youtube.com##ytd-search #contents ytd-reel-shelf-renderer
m.youtube.com##ytm-reel-shelf-renderer
m.youtube.com##ytm-pivot-bar-renderer div.pivot-shorts:upward(ytm-pivot-bar-item-renderer)
m.youtube.com##ytm-browse ytm-item-section-renderer ytm-thumbnail-overlay-time-status-renderer[data-style="SHORTS"]:upward(ytm-video-with-context-renderer)
m.youtube.com##ytm-browse ytm-item-section-renderer ytm-thumbnail-overlay-time-status-renderer[data-style="SHORTS"]:upward(ytm-compact-video-renderer)
m.youtube.com##ytm-search ytm-thumbnail-overlay-time-status-renderer[data-style="SHORTS"]:upward(ytm-compact-video-renderer)
m.youtube.com##ytm-single-column-watch-next-results-renderer ytm-thumbnail-overlay-time-status-renderer span:has-text(/^(0:\d\d|1:0\d)$/):upward(ytm-video-with-context-renderer)

! ===== Mixes & Radios ===== !
www.youtube.com##ytd-browse ytd-rich-item-renderer:has(#video-title-link[href*="&start_radio=1"])
www.youtube.com##ytd-search ytd-radio-renderer
www.youtube.com##ytd-watch-next-secondary-results-renderer ytd-compact-radio-renderer
www.youtube.com##ytd-player div.videowall-endscreen a[data-is-list=true]

! ===== Upcoming & Streams ===== !
www.youtube.com##ytd-browse ytd-grid-video-renderer:has(ytd-thumbnail-overlay-time-status-renderer[overlay-style="UPCOMING"])
www.youtube.com##ytd-browse ytd-rich-item-renderer:has(ytd-thumbnail-overlay-time-status-renderer[overlay-style="UPCOMING"])
www.youtube.com##ytd-browse[page-subtype="subscriptions"] ytd-video-renderer ytd-thumbnail-overlay-time-status-renderer[overlay-style="UPCOMING"]:upward(ytd-item-section-renderer)

! ===== Recommendations ===== !
www.youtube.com##ytd-browse[page-subtype="home"]
www.youtube.com###related
m.youtube.com##ytm-watch ytm-item-section-renderer[data-content-type="related"]
m.youtube.com##ytm-browse div[tab-identifier="FEwhat_to_watch"]
www.youtube.com##.ytp-ce-element
www.youtube.com##.ytp-endscreen-content

! ===== Distractors ===== !
www.youtube.com###comments #contents:remove()

! ===== Search ===== !
www.youtube.com##ytd-search ytd-item-section-renderer ytd-shelf-renderer:style(border: 2px dashed red !important)
www.youtube.com##ytd-search ytd-item-section-renderer ytd-horizontal-card-list-renderer:style(border: 2px dashed red !important)
www.youtube.com##ytd-shelf-renderer.ytd-item-section-renderer.style-scope > .ytd-shelf-renderer.style-scope

! ===== Ministry of Truth ===== !
www.youtube.com###clarify-box

! ===== Anti-Anti-Ad-Blocker ===== !
youtube.com##.ytp-ad-overlay-text

!{
youtube.com##+js(set, yt.config_.openPopupConfig.supportedPopups.adBlockMessageViewModel, false)
youtube.com##+js(set, Object.prototype.adBlocksFound, 0)
youtube.com##+js(set, ytplayer.config.args.raw_player_response.adPlacements, [])
youtube.com##+js(set, Object.prototype.hasAllowedInstreamAd, true)
!}
  ! Sourced from: https://www.followchain.org/video-player-will-be-blocked-youtube/




! =============================== !
! =========== CREDITS =========== !
! =============================== !
! For extra help on YouTube: https://letsblock.it/




! =================================== !
! =========== UNORGANIZED =========== !
! =================================== !
! Unorganized things that I've added with element picker and element zapper
